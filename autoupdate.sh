#!/bin/sh

source ~/.autoupdaterc

# allow to request a reboot with `touch /boot/reboot`
if [ -e /boot/reboot ]; then
    echo "Reboot requested..."
    rm /boot/reboot
    reboot
fi

(cd $TARGET

 BRANCH=`git rev-parse --abbrev-ref HEAD`
 echo "Updating repository (branch: $BRANCH)..."
 git fetch origin $BRANCH
 git reset --hard origin/$BRANCH
 sync
 echo "Update completed."

 # start
 ./autorun.sh)

# defer restart
echo 'Exited. Restarting in 5s...'
sleep 5
