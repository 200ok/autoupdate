#!/bin/sh

REPO=$1
NAME=$2

cd $HOME

# write the config
echo "TARGET=$HOME/$NAME" > .autoupdaterc

# clone the target repo
git clone $REPO $NAME

# where does autoupdate live
SELF=$HOME/autoupdate/autoupdate.sh
USER=`id -un`
GROUP=`id -gn`

# write systemd service file
cat > autoupdate.service <<EOF
[Unit]
Description=Autoupdate
After=graphical.target

[Service]
Type=simple
ExecStart=$SELF
Restart=always
RestartSec=1
User=$USER
Group=$GROUP
EOF

# enable & start
sudo cp autoupdate.service /etc/systemd/system
sudo systemctl enable autoupdate
sudo systemctl start autoupdate
